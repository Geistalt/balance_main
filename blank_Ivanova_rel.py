from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import time
import unittest
import HTMLTestRunner
import sys
import requests
import shutil

###
#global
driver = webdriver.Chrome()
driver.get('https://partnerka.project30.pro/')
driver.maximize_window()
wait = WebDriverWait(driver, 500)

countIU = 2
countAZS = 2
countCP = 2


with open(r"C:\Users\MSSidorkin\Documents\Flash\upload_docs\variable_Ivanova.txt") as file:
    array = [row.strip() for row in file]


class Selenium1_test_Pilot(unittest.TestCase):
    def test001_Login(self):
        driver.find_element_by_name('login').send_keys('maxim.sidorkin@project30.pro')
        driver.find_element_by_name('password').send_keys('$doNheX8'+Keys.RETURN)
        time.sleep(2)
        print('Проходим процедуру авторизации')
        driver.find_element_by_class_name('PageHelpVideo')
        driver.find_element_by_xpath(
            "//div[@class='FmButtonClose__icon -wait-no FmButtonClose__icon--size-medium']").click()
        _ = wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@class='FmButtonLabel__wrap']")))
        driver.find_element_by_xpath("//div[@class='FmButtonLabel__wrap']").click()
        _ = wait.until(EC.element_to_be_clickable((By.XPATH, "(//INPUT[@type='text'])[1]")))
        time.sleep(1)

    def test002_CorrectCreateRequest(self):
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[0]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[1]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[2]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[3])
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print('Заполняем поля корректно, и переходим к разделу "Паспортные данне"')

    def test003_CorrectCreatePassportData(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[5])  # серия и номер паспорта array[5] 7911609375
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[7])  # дата выдачи
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[9])      # код подразделения
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[63])      # место рождения
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[11])  # дата рождения
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").click()                  # пол
        driver.find_element_by_xpath("//DIV[@class='text'][text()='Женский']").click()
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(array[88])  # адрес проживания
        time.sleep(1)   # 3
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(Keys.ENTER)
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[8]").send_keys('+7 (816) 522-32-45')
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(Keys.PAGE_DOWN)
        time.sleep(1)
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Работа"')
        print(' Извлекаем номер заявки')
        # draw
        draw = driver.find_element_by_xpath("//*[text()[contains(.,'Заявка №')]]")
        print('Полное название - ', draw.text)
        _ = draw.text[8:12]   # 8/12
        print('Только номер - ', _)

    def test004_TryCatchModalWindow(self):
        _ = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.FmButtonClose__icon.-wait-no.FmButtonClose__icon--size-medium")))
        try:
            driver.find_element_by_xpath("//DIV[@class='FmButtonClose__icon -wait-no FmButtonClose__icon--size-medium']").click()
            print('Модальное окно "Распечайте форму согласия на обработку персональных данных" появилось и было закрыто')
        except:
            print("Модального окна не появилось")
        time.sleep(1)

    def test005_CorrectCreateWork(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[13]+Keys.ENTER)  # Форма занятости
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[15]+Keys.ENTER)            # Отрасль работодателя
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[17])                    # ИНН
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(Keys.ARROW_DOWN + Keys.ENTER)                    # ИНН
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[19])                # Офиц. номер телефона
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[21]+Keys.ENTER)               # Стаж в текущем месте
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").send_keys(array[23]+Keys.ENTER)              # Квалификация
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(array[25])                       # Доход в месяц в руб.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[8]").send_keys(array[27]+Keys.ENTER)          # Кем приходится клиенту
        driver.find_element_by_xpath("(//INPUT[@type='text'])[9]").send_keys(array[29])            # Имя и отчество контактного лица
        driver.find_element_by_xpath("(//INPUT[@type='text'])[10]").send_keys(array[31])               # Телефон контактоного лица
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Дополнительная информация"')

    def test006_CorrectAddInfo(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[33])     # Образование
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(Keys.ARROW_DOWN + Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[35])     # Серия и номер в/у
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[37])     # Дата выдачи в/у
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[39])     # Семейный статус
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(Keys.ENTER)
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[82])     # Количество лиц на иждивении
        time.sleep(1)
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Параметры кредита и ТС"')

    def test007_CorrectCreateCredit(self):
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[1]").send_keys(array[41])     # Стоимость ТС, руб.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[2]").send_keys(array[43])     # Первоначальный взнос, руб.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[3]").send_keys(array[45])     # Срок кредита, мес.
        driver.find_element_by_xpath("(//INPUT[@type='text'])[4]").send_keys(array[47])     # Комфортный платёж, руб.
        time.sleep(1)
        # Информация об автосалоне и ТС
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").click()
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(array[49])
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[5]").send_keys(Keys.ENTER)
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").click()
        driver.find_element_by_xpath("(//INPUT[@type='text'])[6]").send_keys(array[51]+Keys.ENTER)          # Б/У
        driver.find_element_by_xpath("(//INPUT[@type='text'])[7]").send_keys(array[53])                     # Серия и номер ПТС
        driver.find_element_by_xpath("(//INPUT[@type='text'])[8]").send_keys(array[55])                     # VIN автомобиля
        driver.find_element_by_xpath("(//INPUT[@type='text'])[9]").send_keys(array[57]+Keys.ENTER)          # Марка
        driver.find_element_by_xpath("(//INPUT[@type='text'])[10]").send_keys(array[59]+Keys.ENTER)         # Модель
        time.sleep(1)
        # Есть услуги страхования
        driver.find_element_by_xpath("//DIV[@class='FmSwitch__text  -disabled-no -active-no -focus-no -check-no -wait-no'][text()='Нет']").click()
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[11]").click()     # Тип страхования
        time.sleep(0.5)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[11]").send_keys(array[96]+Keys.ENTER)
        driver.find_element_by_xpath("(//INPUT[@type='text'])[12]").send_keys("15000")      #
        time.sleep(0.5)
        #driver.find_element_by_xpath("//DIV[@class='FmSwitch__text  -disabled-no -active-no -focus-no -check-no -wait-no'][text()='Нет']").click()
        time.sleep(1)
        driver.find_element_by_class_name('FmButtonNext__icon').click()
        print(' Заполняем поля корректно, и переходим к разделу "Сбор документов"')

    def test008_UploadDocs(self):
        # self.skipTest(self)
        # загружаем документы
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[1]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\Photo_Iv.jpg')
        time.sleep(0.5)
        wait.until(EC.invisibility_of_element_located((By.XPATH, "//DIV[@class='FormRequestAvatar__progress']")))
        print("Загружено фото пользователя")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\1pass_Iv.jpg')
        print("Загружен скан паспорта №1")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\2pass_Iv.jpg')
        print("Загружен скан паспорта №2")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\3pass_Iv.jpg')
        print("Загружен скан паспорта №3")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\4pass_Iv.jpg')
        print("Загружен скан паспорта №4")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys\
            (r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\5pass_Iv.jpg')
        print("Загружен скан паспорта №5")
        time.sleep(2)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\6pass_Iv.jpg')
        print("Загружен скан паспорта №6")
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\7pass_Iv.jpg')
        print("Загружен скан паспорта №7")
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\8pass_Iv.jpg')
        print("Загружен скан паспорта №8")
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\9pass_Iv.jpg')
        print("Загружен скан паспорта №9")
        time.sleep(1)
        driver.find_element_by_xpath("(//INPUT[@type='file'])[2]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\10pass_Iv.jpg')
        print("Загружен скан паспорта №10")
        time.sleep(1)
# загружаем скан согласия на обработку персональных данных
        driver.find_element_by_xpath("(//INPUT[@type='file'])[4]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\согл_Субб.pdf')
        print("Загружено согласие на обработку персональных данных")
        time.sleep(3)
# загружаем ПТС
        driver.find_element_by_xpath("(//INPUT[@type='file'])[5]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\ПТС_toyota_prius_MR.jpg')
        print("Загружен ПТС")
        time.sleep(3)
# загружаем водительское удостоверение
        driver.find_element_by_xpath("(//INPUT[@type='file'])[3]").send_keys(
            r'C:\Users\MSSidorkin\Documents\Flash\upload_docs\Dl2_Iv.jpg')
        print("Загружено ВУ")
        time.sleep(2)
        wait.until(EC.visibility_of_element_located((By.XPATH,
                                                     "//*[text()[contains(.,'Набор обязательных документов загружен. "
                                                     "Можно отправлять на проверку в банк.')]]")))

        try:
            driver.find_element_by_css_selector('div.FormRequestFile__name.-error')
            print('ОШИБКА ЗАГРУЗКИ ФОТО!')
            self.fail(unittest.TestCase(driver.close()))
        except:
            print("Ошибки загрузки фото не обнаружено")


        print('Извлекаем номер заявки')
        draw = driver.find_element_by_xpath("//*[text()[contains(.,'Заявка №')]]").text
        global num
        num = draw[8:12]
        print(num)

# отправляем заявку в банк
        driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
        time.sleep(5)
        driver.find_element_by_xpath("//DIV[@class='FmButtonNext__wrap'][text()='Отправить заявку в банк']").click()

    def test009_Verification(self):
        #self.skipTest(self)
        time.sleep(1)
        driver.execute_script("window.open('https://verification-staging.project30.pro/admin/','_blank');")
        driver.switch_to.window(driver.window_handles[-1])

    def test010_LetMeIn(self):
        #self.skipTest(self)
        driver.find_element_by_id('username').send_keys('maxim.sidorkin')
        driver.find_element_by_id('password').send_keys('Moji78vixteR' + Keys.RETURN)
        _ = wait.until(EC.element_to_be_clickable((By.NAME, "query")))

    def test011_Passport(self):
        #self.skipTest(self)
        try:
            driver.find_element_by_name('query').send_keys(num+'15' + Keys.RETURN)
            time.sleep(3)
            string = driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]")
            print(string.text)
        except:
            print('Element not found')
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(1)
        # первый скан
        driver.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').send_keys(array[5])    #array[5]
        time.sleep(0.5)
        driver.find_element_by_id('firstSpread').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[2]").click()
        driver.find_element_by_id('signature').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[3]").click()
        driver.find_element_by_id('registrationFirst').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[4]").click()
        driver.find_element_by_id('registrationSecond').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[5]").click()
        driver.find_element_by_id('registrationThird').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[6]").click()
        driver.find_element_by_id('registrationFourth').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[7]").click()
        driver.find_element_by_id('militaryDuty').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[8]").click()
        driver.find_element_by_id('maritalStatus').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[9]").click()
        driver.find_element_by_id('children').click()

        driver.find_element_by_xpath("(//DIV[@class='thumbnail__image'])[10]").click()
        driver.find_element_by_id('previouslyIssued').click()
        time.sleep(1)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//DIV[@class='Button__content']")))
        driver.find_element_by_xpath("//DIV[@class='Button__content']").click()
        time.sleep(1)
        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test012_InputData(self):
        self.skipTest(self)

    def test013_NoName(self):
        # self.skipTest(self)
        time.sleep(1)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '16' + Keys.RETURN)
        time.sleep(0.5)
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(1.5)
        for element in driver.find_elements_by_class_name('Switch__right'):
            element.click()
        time.sleep(0.5)
        wait.until(EC.element_to_be_clickable((By.ID, "lastName")))
        driver.find_element_by_id('lastName').send_keys(array[0])
        driver.find_element_by_id('firstName').send_keys(array[1])
        driver.find_element_by_id('secondName').send_keys(array[2])
        driver.find_element_by_id('birthday').send_keys(array[11])
        driver.find_element_by_id('birthPlace').send_keys(array[63])
        time.sleep(0.5)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//SPAN[@class='Button__label'][text()='Готово']")))
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()
        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены ()')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

        # street
    def test014_InputDL(self):
        #self.skipTest(self)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num+'17' + Keys.RETURN)
        time.sleep(1)
        ####
        #####
        try:
            e = driver.find_element_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]")
            print('Found', e)
        except:
            print(' Not found')
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
            #####
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('registrationAddress').send_keys(array[63])
        time.sleep(4)
        #driver.find_element_by_class_name("suggestions__item suggestions__item--active")
        driver.find_element_by_class_name("Input__suggestions").click()
        time.sleep(1)
        driver.find_element_by_id('street').send_keys('1')

        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены ()')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

# start
    def test015_InputPTS(self):
        #self.skipTest(self)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num+'18' + Keys.RETURN)
        time.sleep(1)

        #####
        try:
            e = driver.find_element_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]")
            print('Found', e)
        except:
            print(' Not found')
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
            #####

        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)

        driver.find_element_by_id('issuedBy').send_keys(array[61])
        driver.find_element_by_id('issuedAt').send_keys(array[7])
        driver.find_element_by_id('divisionCode').send_keys(array[9])
        time.sleep(1)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ПТС)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test016_Photo(self):
        #self.skipTest(self)
        time.sleep(1)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '19' + Keys.RETURN)
        time.sleep(0.5)
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('correspondsToExpectedType--true').click()
        driver.find_element_by_id('wellReadableAndHasNoDefects--true').click()
        driver.find_element_by_id('corporateTemplateUsed--true').click()
        driver.find_element_by_id('thereIsAPersonalDataProcessingConsentSignature--true').click()
        driver.find_element_by_id('thereIsACreditHistoryRequestSignature--true').click()
        time.sleep(0.5)
        driver.find_element_by_id('INPUT_PASSPORT_SERIES_NUMBER').send_keys(array[5])    #array[5]
        time.sleep(0.5)
        driver.find_element_by_xpath("//DIV[@class='Button__content']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ФОТО)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test017_NoName(self):
        #self.skipTest(self)
        time.sleep(1)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '18' + Keys.RETURN)
        time.sleep(0.5)
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(1.5)
        driver.find_element_by_id('issuedBy').send_keys(array[61])
        time.sleep(1)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ФОТО)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test018_NoName(self):
        #self.skipTest(self)
        time.sleep(1)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '17' + Keys.RETURN)
        time.sleep(0.5)
        driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('registrationAddress').send_keys(array[63])
        time.sleep(4)
        driver.find_element_by_class_name("Input__suggestions").click()
        time.sleep(1)
        driver.find_element_by_id('street').send_keys('1')

        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()
        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ФОТО)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test019_NoName(self):
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '03' + Keys.RETURN)
        time.sleep(1)
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
        # driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('correspondsToExpectedType--true').click()  # Документ является в/у
        driver.find_element_by_id(
            'wellReadableAndHasNoDefects--true').click()  # Хорошо читается, дефектов скан. нет
        driver.find_element_by_xpath("//LABEL[@class='CheckBox__label']").click()  # Отметить как скан с фото
        driver.find_element_by_id('issuedAt').send_keys(array[37])  # Дата выдачи
        driver.find_element_by_id('INPUT_DRIVER_LICENSE_SERIES_NUMBER').send_keys(array[35])  # серия и номер ВУ
        time.sleep(0.5)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ВУ)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test020_NoName(self):
        # self.skipTest(self)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '04' + Keys.RETURN)
        time.sleep(1)
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')

        # driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
        time.sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('correspondsToExpectedType--true').click()  # Документ является ПТС
        driver.find_element_by_id(
            'wellReadableAndHasNoDefects--true').click()  # Хорошо читается, дефектов скан. нет
        driver.find_element_by_id('inputVehiclePassportSeriesNumber').send_keys(array[53])  # Серия и номер ПТС
        driver.find_element_by_id('vin').send_keys(array[55])  # VIN
        driver.find_element_by_id('brand').send_keys(array[57])  # Марка
        driver.find_element_by_id('model').send_keys(array[59])  # Модель
        driver.find_element_by_id('year').send_keys('2017')  # Год выпуска
        driver.find_element_by_id('enginePower').send_keys(array[69])  # Мощность
        driver.find_element_by_id('engineCapacity').send_keys(array[67])  # Объем двигателя, см³
        driver.find_element_by_id('engineType--0').click()  # Тип двигателя

        driver.find_element_by_xpath("(//DIV[@class='RadioButton__check'])[2]").click()
        driver.find_element_by_xpath("(//DIV[@class='RadioButton__check'])[4]").click()

        time.sleep(1)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ПТС)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])

    def test021_NoName(self):
        # self.skipTest(self)
        time.sleep(0.5)
        driver.find_element_by_name('query').clear()
        time.sleep(1)
        driver.find_element_by_name('query').send_keys(num + '05' + Keys.RETURN)
        time.sleep(3)
        #####
        try:
            e = driver.find_element_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]")
            print('Found', e)
        except:
            print(' Not found')
        while driver.find_elements_by_xpath("//*[text()[contains(.,'Ничего не найдено')]]"):
            time.sleep(1)
            driver.find_element_by_name('query').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath("//*[text()[contains(.,'Взять себе')]]").click()
            print('Нашел и вышел из цикла')
        #####
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(0.5)
        driver.find_element_by_id('left-scan--true').click()
        driver.find_element_by_id('right-scan--true').click()
        time.sleep(1)
        driver.find_element_by_xpath("//SPAN[@class='Button__label'][text()='Готово']").click()

        try:
            time.sleep(2)
            driver.find_element_by_xpath("//DIV[@class='Wait__message-text'][text()='Все документы проверены']")
            print('Все документы проверены (ФОТО)')
        except:
            print('ОШИБКА!')

        driver.close()
        time.sleep(0.5)
        driver.switch_to.window(driver.window_handles[-1])




if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Selenium1_test_Pilot))

    buf = open("at_for_blank_Subbotina_SL.html", 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(
        stream=buf,
        title='ПРОВЕРКА СОСТАВЛЕНИЯ ЗАВЯКИ И ВЕРИФИКАЦИИ ДЛЯ МАМЛЕЕВА (Страхование жизни)',
        description='Отчет по тестированию'
    )
    ret = not runner.run(suite).wasSuccessful()
    sys.exit(ret)
